package de.htw.ai.kbe.servlet.resources;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import de.htw.ai.kbe.servlet.business.IAuthenticationService;
import de.htw.ai.kbe.servlet.utils.Constants;

@Path(Constants.AUTH_RESOURCE_PATH)
public class AuthResource {
    
    @Inject
    private IAuthenticationService authService;

    public AuthResource() {
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getToken(@QueryParam("userId") String userId) {
        if(userId == null || userId.isEmpty()) {
            throw new BadRequestException("No userId given");
        }
        
        String token = authService.authenticate(userId);
        return token;
    }
}
