package de.htw.ai.kbe.servlet.resources.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import de.htw.ai.kbe.servlet.business.IAuthenticationService;
import de.htw.ai.kbe.servlet.utils.Constants;

@Provider
public class TokenFilter implements ContainerRequestFilter {

    // use provider for lazy retrievement of injected instance
    // https://stackoverflow.com/a/32075904
    // (fix for 'not in requestscope' error caused by injected EntityManager)
    @Inject
    private javax.inject.Provider<IAuthenticationService> authService;
    
    @Override
    public void filter(ContainerRequestContext req) throws IOException {
        String path = req.getUriInfo().getPath();
        if(path.contains(Constants.AUTH_RESOURCE_PATH.substring(1))) {
            return;
        }
        
        String token = req.getHeaderString(Constants.AUTH_HEADER);
        
        try {
            String userId = authService.get().verify(token);
            
            // Inject user id in request context for reading in resources
            req.setProperty(Constants.CONTEXT_USER_ID, userId);
        } catch (NotAuthorizedException e) {
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
    }

}
