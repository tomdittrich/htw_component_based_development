package de.htw.ai.kbe.servlet.business;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;

import org.apache.log4j.Logger;

import de.htw.ai.kbe.servlet.persistence.entities.Token;
import de.htw.ai.kbe.servlet.persistence.entities.User;
import de.htw.ai.kbe.servlet.persistence.token.ITokenPersistence;
import de.htw.ai.kbe.servlet.persistence.user.IUserPersistence;

public class AuthenticationServiceImpl implements IAuthenticationService {

    private static final Logger LOG = Logger.getLogger(AuthenticationServiceImpl.class);

    @Inject
    private IUserPersistence userPersistence;

    @Inject
    private ITokenPersistence tokenPersistence;

    public AuthenticationServiceImpl() {
    }

    @Override
    public synchronized String authenticate(String userId) throws NotAuthorizedException {
        LOG.info("Authenticating user " + userId);
        
        User user;
        try {
            user = userPersistence.getByUserId(userId);
        } catch (NotFoundException e) {
            throw new NotAuthorizedException("User does not exist");
        }

        Token t = tokenPersistence.findTokenByUser(user);
        
        String token = generateToken(user);

        if(t != null) {
            // Update token entry
            t.setToken(token);
            tokenPersistence.updateToken(t);
        } else {
            
            // Create new token
            t = new Token();
            t.setToken(token);
            t.setUser(user);
            
            tokenPersistence.persistToken(t);
        }

        return token;
    }

    private String generateToken(User user) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            byte[] hash = md.digest((user.getId() + UUID.randomUUID().toString()).getBytes(StandardCharsets.UTF_8));

            return Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            // In case SHA-256 is not found, something's really really wrong.
            throw new RuntimeException(e);
        }
    }

    @Override
    public String verify(String token) throws NotAuthorizedException {
        Token t = tokenPersistence.findToken(token);
        if(t == null || t.getUser() == null) {
            throw new NotAuthorizedException("Token invalid");
        }
        return t.getUser().getId();
    }
}