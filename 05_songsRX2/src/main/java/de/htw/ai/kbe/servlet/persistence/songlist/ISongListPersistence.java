package de.htw.ai.kbe.servlet.persistence.songlist;

import de.htw.ai.kbe.servlet.persistence.entities.SongList;
import de.htw.ai.kbe.servlet.persistence.entities.User;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Interface for handling persistence operations for {@link SongList}s. <br>
 * General interface for datastorage
 *
 * @author marcel munce
 */
public interface ISongListPersistence {

    /**
     * @return List of all songlists from a specific user
     */
    public List<SongList> getListsOfUser(User user);

    /**
     * Retrieve a {@link SongList} by specific id
     *
     * @param listId id of songlist
     * @param user   user of songlist
     * @return object if found
     * @throws NoSuchElementException in case no object with given Id exists
     */
    public SongList getListByIdAndUser(int listId, User user) throws NoSuchElementException;

    /**
     * Removes a {@link SongList} from the data store
     *
     * @param list item to delete
     * @throws NoSuchElementException in case no object with given Id exists
     */
    public void delete(SongList list) throws NoSuchElementException;

    /**
     * Saves a {@link SongList} in the data store.
     *
     * @param list item to store
     */
    public void persist(SongList list);
}
