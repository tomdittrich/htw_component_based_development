package de.htw.ai.kbe.servlet.resources;

import static de.htw.ai.kbe.servlet.utils.Utils.buildErrorResponse;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import de.htw.ai.kbe.servlet.persistence.entities.Song;
import de.htw.ai.kbe.servlet.persistence.entities.SongList;
import de.htw.ai.kbe.servlet.persistence.entities.User;
import de.htw.ai.kbe.servlet.persistence.song.ISongPersistence;
import de.htw.ai.kbe.servlet.persistence.songlist.ISongListPersistence;
import de.htw.ai.kbe.servlet.persistence.user.IUserPersistence;
import de.htw.ai.kbe.servlet.utils.Constants;

/**
 * Servlet to read and write Songlists into a simple kind of database
 *
 * @version 0.1
 */
@Path(Constants.SONGLIST_RESOURCE_PATH)
public class SongListResource {

    private static final Logger LOG = Logger.getLogger(SongListResource.class);

    @Inject
    private IUserPersistence userPersistence;

    @Inject
    private ISongListPersistence songListPersistence;

    @Inject
    private ISongPersistence songPersistence;

    @Inject
    private ContainerRequestContext ctx;

    @GET
    @Path("/{listId}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getListById(@PathParam("userId") String userId, @PathParam("listId") int listId) {
        LOG.info("Requesting songlist of user " + userId + " with id " + listId);
        try {
            User u = userPersistence.getByUserId(userId);

            SongList list = songListPersistence.getListByIdAndUser(listId, u);
            if (isPrivate(list)) {
                return buildErrorResponse(Status.UNAUTHORIZED, "Private List");
            }

            return Response.ok(list).build();

        } catch (NoSuchElementException | NoResultException e) {
            LOG.debug("List not found");
            return buildErrorResponse(Status.NOT_FOUND, "List for user not found");
        } catch (NotFoundException e) {
            return buildErrorResponse(Status.NOT_FOUND, "User not found");
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getListsOfUser(@PathParam("userId") String userId) {
        LOG.info("Requesting songlists of user " + userId);
        try {
            User u = userPersistence.getByUserId(userId);

            List<SongList> lists = songListPersistence.getListsOfUser(u);

            lists = lists.stream().filter(l -> !isPrivate(l)).collect(Collectors.toList());

            // Wrap list of results in generic entity for preventing type loss
            // see http://www.adam-bien.com/roller/abien/entry/jax_rs_returning_a_list
            GenericEntity<List<SongList>> generic = new GenericEntity<List<SongList>>(lists) {
            };

            return Response.ok(generic).build();

        } catch (NotFoundException | NoResultException e) {
            LOG.debug("User not found");
            return buildErrorResponse(Status.NOT_FOUND, "User not found");
        }
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response addlistForUser(@PathParam("userId") String userId, SongList songlist, @Context UriInfo uriInfo) {
        LOG.info("Creating list for user " + userId);
        User u;
        try {
            u = userPersistence.getByUserId(userId);
        } catch (NotFoundException e) {
            return buildErrorResponse(Status.NOT_FOUND, "User not found");
        }

        if (!u.getId().equals((String) ctx.getProperty(Constants.CONTEXT_USER_ID))) {
            return buildErrorResponse(Status.UNAUTHORIZED, "Creation of lists for other users not allowed");
        }

        songlist.setOwner(u);

        try {
            URI uri = processSongList(songlist, uriInfo);
           
            return Response.created(uri).entity("List added (new id: " + songlist.getId() + ")").build();

        } catch (NoSuchElementException | PersistenceException | IllegalArgumentException e) {
            return buildErrorResponse(Status.BAD_REQUEST, "Could not add Songlist. " + e.getMessage());
        }
    }
    
    private URI processSongList(SongList songlist, UriInfo uriInfo) {
        if(songlist.getSongs() == null || songlist.getSongs().isEmpty()) {
            throw new IllegalArgumentException("No songs given");
        }
        
        // Get Songs from database by id
        List<Song> songs = songlist.getSongs().stream()
                .map(s -> {
                    if(s == null || s.getId() == null) { throw new IllegalArgumentException("Invalid song payload"); }
                    return songPersistence.getById(s.getId()); }).collect(Collectors.toList());

        songlist.setSongs(songs);

        songListPersistence.persist(songlist);

        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(Integer.toString(songlist.getId()));
        
        return uriBuilder.build();
    }

    @DELETE
    @Path("/{listId}")
    public Response deleteListOfuser(@PathParam("userId") String userId, @PathParam("listId") int listId) {
        LOG.info("Deleting list " + listId + " of user " + userId);
        User u;
        try {
            u = userPersistence.getByUserId(userId);
        } catch (NotFoundException e) {
            return buildErrorResponse(Status.NOT_FOUND, "User not found");
        }

        try {
            SongList list = songListPersistence.getListByIdAndUser(listId, u);

            if (!u.getId().equals((String) ctx.getProperty(Constants.CONTEXT_USER_ID))) {
                return buildErrorResponse(Status.UNAUTHORIZED, "Private list or not enough permissions");
            }

            songListPersistence.delete(list);

            return Response.noContent().build();

        } catch (NoSuchElementException | NoResultException e) {
            LOG.debug("List not found");
            return buildErrorResponse(Status.NOT_FOUND, "List for user not found");
        } catch (PersistenceException e) {
            return buildErrorResponse(Status.BAD_REQUEST, "Could not delete the songlist." + e.getMessage());
        }
    }
    
    private boolean isPrivate(SongList list) {
        String requestingUser = (String) ctx.getProperty(Constants.CONTEXT_USER_ID);
        return !(list.isPublic() || list.getOwner().getId().equals(requestingUser));
    }
}
