package de.htw.ai.kbe.servlet.di;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.process.internal.RequestScoped;

import de.htw.ai.kbe.servlet.business.AuthenticationServiceImpl;
import de.htw.ai.kbe.servlet.business.IAuthenticationService;
import de.htw.ai.kbe.servlet.persistence.song.ISongPersistence;
import de.htw.ai.kbe.servlet.persistence.song.SongDao;
import de.htw.ai.kbe.servlet.persistence.songlist.ISongListPersistence;
import de.htw.ai.kbe.servlet.persistence.songlist.SongListDao;
import de.htw.ai.kbe.servlet.persistence.token.ITokenPersistence;
import de.htw.ai.kbe.servlet.persistence.token.TokenDao;
import de.htw.ai.kbe.servlet.persistence.user.IUserPersistence;
import de.htw.ai.kbe.servlet.persistence.user.UserDao;
import de.htw.ai.kbe.servlet.utils.Constants;
import de.htw.ai.kbe.servlet.utils.InjectableEMFFactory;
import de.htw.ai.kbe.servlet.utils.InjectableEMFactory;

/**
 * Injects the dependencies for the servlet.<br>
 * Here we use a real database (and all the DAOs) as a data store
 */
public class DependencyBinder extends AbstractBinder {

    @Override
    protected void configure() {
        bindFactory(new InjectableEMFFactory(Constants.SONG_PERSISTENCE_UNIT)).to(EntityManagerFactory.class).in(Singleton.class);
        bindFactory(InjectableEMFactory.class).to(EntityManager.class).in(RequestScoped.class);
        
        bind(SongDao.class).to(ISongPersistence.class);
        bind(UserDao.class).to(IUserPersistence.class);
        bind(TokenDao.class).to(ITokenPersistence.class);
        bind(SongListDao.class).to(ISongListPersistence.class);
        bind(AuthenticationServiceImpl.class).to(IAuthenticationService.class);
    }

}
