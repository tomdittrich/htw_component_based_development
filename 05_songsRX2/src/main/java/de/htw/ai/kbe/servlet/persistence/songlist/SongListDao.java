package de.htw.ai.kbe.servlet.persistence.songlist;

import java.util.List;
import java.util.NoSuchElementException;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import de.htw.ai.kbe.servlet.persistence.entities.SongList;
import de.htw.ai.kbe.servlet.persistence.entities.User;

/**
 * Implementation of {@link ISongListPersistence} for {@link SongList} objects.<br>
 * Songslists were stored in a real Database like MySQL
 *
 * @author marcel munce
 */
public class SongListDao implements ISongListPersistence {

    @Inject
    private EntityManager em;

    @Override
    public SongList getListByIdAndUser(int listId, User user) throws NoSuchElementException {
        Query q = em.createQuery("SELECT l FROM SongList l WHERE l.id = :id AND l.owner = :user");
        q.setParameter("id", listId);
        q.setParameter("user", user);
        try {
            return (SongList) q.getSingleResult();
        } catch (NoResultException e) {
            throw new NoSuchElementException("No list with id " + listId + " for user '" + user.getId() + "' found");
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SongList> getListsOfUser(User user) {
        Query q = em.createQuery("SELECT l FROM SongList l WHERE l.owner = :user");
        q.setParameter("user", user);
        return q.getResultList();
    }

    @Override
    public void delete(SongList list) throws NoSuchElementException, PersistenceException {
        try {
            em.getTransaction().begin();
            em.remove(list);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new PersistenceException("Could not persist entity: " + e.toString());
        }
    }

    @Override
    public void persist(SongList list) throws PersistenceException{
        try {
            em.getTransaction().begin();
            em.persist(list);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new PersistenceException("Could not persist entity: " + e.toString());
        }
    }

}
