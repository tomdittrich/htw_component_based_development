package de.htw.ai.kbe.servlet.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.glassfish.hk2.api.Factory;

public class InjectableEMFFactory implements Factory<EntityManagerFactory>{

    private final EntityManagerFactory emf;

    public InjectableEMFFactory(String persistenceUnitName) {
        this.emf = Persistence.createEntityManagerFactory(persistenceUnitName);
    }
    
    @Override
    public void dispose(EntityManagerFactory emf) {
        emf.close();
    }

    @Override
    public EntityManagerFactory provide() {
        return emf;
    }
}
