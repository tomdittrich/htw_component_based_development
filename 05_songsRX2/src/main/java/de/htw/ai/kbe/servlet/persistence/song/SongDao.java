package de.htw.ai.kbe.servlet.persistence.song;

import de.htw.ai.kbe.servlet.persistence.entities.Song;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Implementation of {@link ISongPersistence} for {@link Song} objects.<br>
 * Songs were stored in a real Database like MySQL
 *
 * @author marcel munce
 */
public class SongDao implements ISongPersistence {

    @Inject
    protected EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    public List<Song> getAll() {
        Query query = em.createQuery("SELECT s FROM Song s");
        return query.getResultList();
    }

    @Override
    public Song getById(int id) throws NoSuchElementException {
        Song s = em.find(Song.class, id);
        if (s == null) {
            throw new NoSuchElementException("No song with id " + id);
        }
        return s;
    }

    @Override
    public void delete(int id) throws NoSuchElementException, PersistenceException {
        Song s = getById(id);

        try {
            em.getTransaction().begin();
            // delete song from lists
            s.getLists().stream().forEach(l -> {
                l.getSongs().remove(s);
                em.merge(l);
            });
            em.remove(s);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new PersistenceException("Could not persist entity: " + e.toString());
        }
    }

    @Override
    public int add(Song item) throws PersistenceException {
        try {
            em.getTransaction().begin();
            em.persist(item);
            em.getTransaction().commit();
            return item.getId();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new PersistenceException("Could not persist entity: " + e.toString());
        }
    }

    @Override
    public void update(Song item) throws NoSuchElementException {
        if( item.getId() == null) {
            throw new NoSuchElementException();
        }

        // check if song exists in database
        getById(item.getId());

        em.getTransaction().begin();
        em.merge(item);
        em.getTransaction().commit();
    }

}
