-- Host: db.f4.htw-berlin.de:3306
-- Server version: 5.6.39
-- PHP Version: 5.6.33-0+deb8u1

INSERT INTO `songs` (`id`, `album`, `artist`, `released`, `title`) VALUES
(1, 'Trolls', 'Justin Timberlake', 2016, 'Can''t Stop the Feeling'),
(2, 'Thank You', 'Meghan Trainor, Kelli Trainor', 2016, 'Mom'),
(3, NULL, 'Iggy Azalea', 2016, 'Team'),
(4, 'Ghostbusters', 'Fall Out Boy, Missy Elliott', 2016, 'Ghostbusters (I''m not a fraid)'),
(5, 'Bloom', 'Camila Cabello, Machine Gun Kelly', 2017, 'Bad Things'),
(6, 'At Night, Alone.', 'Mike Posner', 2016, 'I Took a Pill in Ibiza'),
(7, 'Top Hits 2017', 'Gnash', 2017, 'i hate u, i love u'),
(8, 'Thank You', 'Meghan Trainor', 2016, 'No'),
(9, 'Glory', 'Britney Spears', 2016, 'Private Show'),
(10, 'Lukas Graham (Blue Album)', 'Lukas Graham', 2015, '7 Years');

INSERT INTO `users` (`id`, `firstName`, `lastName`) VALUES
('eschuler', 'Elena', 'Schuler'),
('mmuster', 'Maxime', 'Muster');

INSERT INTO `songlists` (`id`, `public`, `owner_id`) VALUES
  (1, b'1', 'mmuster'),
  (2, b'0', 'mmuster'),
  (3, b'1', 'eschuler'),
  (4, b'0', 'eschuler');

INSERT INTO `list_song` (`list_id`, `song_id`) VALUES
  (1, 1),
  (1, 2),
  (2, 3),
  (2, 6),
  (3, 7),
  (3, 10),
  (4, 4),
  (4, 10);