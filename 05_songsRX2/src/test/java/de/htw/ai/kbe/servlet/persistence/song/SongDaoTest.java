package de.htw.ai.kbe.servlet.persistence.song;

import static de.htw.ai.kbe.servlet.utils.TestUtils.assertSong;
import static de.htw.ai.kbe.servlet.utils.TestUtils.getTestSongWithIdOne;
import static de.htw.ai.kbe.servlet.utils.TestUtils.getTestSongWithJustATitle;
import static de.htw.ai.kbe.servlet.utils.TestUtils.getTestSongWithWrongPayload;
import static de.htw.ai.kbe.servlet.utils.TestUtils.getTestSongWithoutId;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import org.h2.tools.RunScript;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.htw.ai.kbe.servlet.persistence.entities.Song;

/**
 * Some tests for the {@link SongDao} class
 *
 * @author Tom Dittrich s0555944@htw-berlin.de
 */
public class SongDaoTest {

    protected static EntityManagerFactory emf;
    protected static EntityManager em;

    private SongDao songPersistence;

    @BeforeClass
    public static void init() {
        // using an H2 in-memory database to simulate a real database
        emf = Persistence.createEntityManagerFactory("song-test");
        em = emf.createEntityManager();
    }

    @Before
    public void initializeDatabase() {
        songPersistence = new SongDao();
        songPersistence.em = em;

        // new dataset for every test case
        Session session = em.unwrap(Session.class);
        session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                try {
                    File script = new File(getClass().getResource("data.sql").getFile());
                    RunScript.execute(connection, new FileReader(script));
                } catch (FileNotFoundException e) {
                    throw new RuntimeException("could not initialize with script");
                }
            }
        });

    }

    @After
    public void clearAfter() {
        em.clear();
    }

    @AfterClass
    public static void tearDown() {
        em.clear();
        em.close();
        emf.close();
    }

    // -------------
    // Get Tests
    // -------------

    @Test
    public void getAllSongShouldReturnSongList() {
        List<Song> songlist = songPersistence.getAll();

        assertEquals(2, songlist.size());
        assertSong(songlist.get(0), 1, "Tool", "Lateralus", "Ticks And Leeches", 2001);
        assertSong(songlist.get(1), 2, "Alice In Chains", "Jar Of Flies", "Rotten Apple", 1994);
    }

    @Test
    public void getSongByIdShouldReturnSong() {
        Song song = songPersistence.getById(2);

        assertSong(song, 2, "Alice In Chains", "Jar Of Flies", "Rotten Apple", 1994);
    }

    @Test(expected = NoSuchElementException.class)
    public void getSongWithNonExistingIdShouldReturnException() {
        Song song = songPersistence.getById(99);
        assertNull(song);
    }

    // -------------
    // Delete Tests
    // -------------

    @Test
    public void deleteSongOneShouldSuccess() {
        songPersistence.delete(1);
        Song actualSong = em.find(Song.class, 1);

        assertNull(actualSong);
    }

    @Test
    public void deleteSongOneShouldStaySongTwoAlive() {
        songPersistence.delete(1);
        Song songTwo = em.find(Song.class, 2);

        assertSong(songTwo, 2, "Alice In Chains", "Jar Of Flies", "Rotten Apple", 1994);
    }

    @Test(expected = NoSuchElementException.class)
    public void deleteNonExistingSongShouldReturnException() {
        songPersistence.delete(99);
    }

    // -------------
    // Post Tests
    // -------------

    @Test
    public void addNewSongShouldSuccessAndReturnIdNumber() {
        Song songToAdd = getTestSongWithoutId();
        int newId = songPersistence.add(songToAdd);
        Song songActual = em.find(Song.class, newId);

        assertSong(songActual, 3, "Jimi Hendrix", "Axis: Bold As Love", "Little Wing", 1967);
    }

    @Test
    public void addNewSongJustWithTitleShouldSuccess() {
        Song songToAdd = getTestSongWithJustATitle();
        int newId = songPersistence.add(songToAdd);
        Song songActual = em.find(Song.class, newId);

        assertSong(songActual, 3, null, null, "Supermix 2000", null);
    }

    @Test(expected = PersistenceException.class)
    public void addNewSongWithWrongPayloadShouldException() {
        Song songToAdd = getTestSongWithWrongPayload();
        songPersistence.add(songToAdd);
    }

    @Test
    public void deleteASongShouldNotAffectNewId() {
        Song songToRemove = em.find(Song.class, 1);

        em.getTransaction().begin();
        em.remove(songToRemove);
        em.getTransaction().commit();

        Song songToAdd = getTestSongWithoutId();
        int newId = songPersistence.add(songToAdd);

        List<Song> liste = songPersistence.getAll();

        for (Song e : liste) {
            System.out.println(e);
        }

        assertEquals(3, newId);
    }

    // -------------
    // Put Tests
    // -------------

    @Test
    public void updateSongOneShouldSuccess() {
        Song songPutTest = getTestSongWithIdOne();

        // the original song
        Song before = em.find(Song.class, 1);
        assertSong(before, 1, "Tool", "Lateralus", "Ticks And Leeches", 2001);

        songPersistence.update(songPutTest);

        // the new overwritten song
        songPutTest = em.find(Song.class, 1);
        assertSong(songPutTest, 1, "Jimi Hendrix", "Axis: Bold As Love", "Little Wing", 1967);
    }

    @Test(expected = NoSuchElementException.class)
    public void updateSongWithNonExistingIdShouldException() {
        Song song = getTestSongWithoutId();

        songPersistence.update(song);
    }

    @Test(expected = NoSuchElementException.class)
    public void updateNonExistingSongShouldException() {
        Song song = getTestSongWithoutId();
        song.setId(99);

        songPersistence.update(song);
    }

    @Test
    public void updateSongJustWithTitleAndIdShouldSuccess() {
        Song songPutTest = getTestSongWithJustATitle();
        songPutTest.setId(1);

        // the original song
        Song before = em.find(Song.class, 1);
        assertSong(before, 1, "Tool", "Lateralus", "Ticks And Leeches", 2001);

        songPersistence.update(songPutTest);

        // the new overwritten song
        songPutTest = em.find(Song.class, 1);
        assertSong(songPutTest, 1, null, null, "Supermix 2000", null);
    }

    @Test(expected = PersistenceException.class)
    public void updateSongWithRightIdButWrongPayloadShouldException() {
        Song songToAdd = getTestSongWithWrongPayload();
        songToAdd.setId(1);
        songPersistence.update(songToAdd);
    }
}