package de.htw.ai.kbe.servlet.utils;

import java.util.ArrayList;
import java.util.List;

import de.htw.ai.kbe.servlet.persistence.entities.Song;

import static org.junit.Assert.assertEquals;

/**
 * Just some helpful utils for the test classes.
 * Primary to purify the test code.
 *
 * @version 0.1
 */
public class TestUtils {

    /**
     * Compares and test an song object against expected values
     *
     * @param song     song object (actual object)
     * @param id       database id (expected)
     * @param artist   artisname (expected)
     * @param album    albumname (expected)
     * @param title    titlename (expected)
     * @param released release year (expected)
     */
    public static void assertSong(Song song, int id, String artist, String album, String title, Integer released) {
        assertEquals(artist, song.getArtist());
        assertEquals(album, song.getAlbum());
        assertEquals(id, song.getId().intValue());
        assertEquals(title, song.getTitle());
        assertEquals(released, song.getReleased());
    }
    
    public static void assertSong(Song expected, Song actual) {
        assertSong(actual, expected.getId(), expected.getArtist(), expected.getAlbum(), expected.getTitle(), expected.getReleased());
    }

    /**
     * Return a list of two given {@link Song}
     *
     * @return list of two songs
     */
    public static List<Song> getTestSongs() {
        List<Song> songList = new ArrayList<>();

        Song song1 = new Song();
        songList.add(song1);

        song1.setAlbum("Lateralus");
        song1.setArtist("Tool");
        song1.setId(1);
        song1.setReleased(2001);
        song1.setTitle("Ticks And Leeches");

        Song song2 = new Song();
        songList.add(song2);

        song2.setAlbum("Jar Of Flies");
        song2.setArtist("Alice In Chains");
        song2.setId(2);
        song2.setReleased(1994);
        song2.setTitle("Rotten Apple");

        return songList;
    }

    /**
     * Return a single {@link Song} with id 1
     *
     * @return song
     */
    public static Song getTestSongWithIdOne() {
        Song song = new Song();
        song.setId(1);
        song.setArtist("Jimi Hendrix");
        song.setAlbum("Axis: Bold As Love");
        song.setTitle("Little Wing");
        song.setReleased(1967);
        return song;
    }

    /**
     * Return a single {@link Song} without id
     *
     * @return song
     */
    public static Song getTestSongWithoutId() {
        Song song = new Song();
        song.setArtist("Jimi Hendrix");
        song.setAlbum("Axis: Bold As Love");
        song.setTitle("Little Wing");
        song.setReleased(1967);
        return song;
    }

    /**
     * Return a single {@link Song} with wrong payload
     *
     * @return song
     */
    public static Song getTestSongWithWrongPayload() {
        Song song = new Song();
        song.setArtist("Pink Floyd");
        return song;
    }

    /**
     * Return a single {@link Song} just with a title
     *
     * @return song
     */
    public static Song getTestSongWithJustATitle() {
        Song song = new Song();
        song.setTitle("Supermix 2000");
        return song;
    }

    /**
     * Return a payload for a JSON song POST submit
     *
     * @return payload
     */
    public static String getJsonPayload() {
        return "{\n  \"title\" : \"Little Wing\",\n  \"artist\" : \"Jimi Hendrixx\",\n  \"album\" : \"Axis: Bold As Love\",\n  \"released\" : 1967\n}";
    }

    /**
     * Return a payload for a XML song POST submit
     *
     * @return
     */
    public static String getXmlPayload() {
        return "<songs>\n" +
                "<song>\n" +
                "  <title>Little Wing</title>\n" +
                "  <artist>Jimi Hendrixx</artist>\n" +
                "  <album>Axis: Bold As Love</album>\n" +
                "  <released>1967</released>\n" +
                "</song>\n" +
                "</songs>";
    }
    
    public static Song copySong(Song toCopy) {
        return new Song.Builder(toCopy.getTitle())
        .album(toCopy.getAlbum())
        .artist(toCopy.getArtist())
        .id(toCopy.getId())
        .released(toCopy.getReleased())
        .build();
    }
}
