package de.htw.ai.kbe.servlet.resources;

import de.htw.ai.kbe.servlet.persistence.entities.Song;
import de.htw.ai.kbe.servlet.persistence.song.ISongPersistence;
import de.htw.ai.kbe.servlet.utils.TestUtils;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.NoSuchElementException;

import static de.htw.ai.kbe.servlet.utils.TestUtils.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

/**
 * Some tests for the {@link SongsResource} class
 *
 * @version 0.1
 */
public class SongsResourceTest extends JerseyTest {

    @Mock
    private ISongPersistence songPersistence;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Override
    protected Application configure() {
        return new ResourceConfig(SongsResource.class).register(
                new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(songPersistence).to(ISongPersistence.class);
                    }
                });
    }

    // -------------
    // get Tests
    // -------------

    @Test
    public void getSongWithValidIdShouldReturnSong() {
        when(songPersistence.getById(1)).thenReturn(TestUtils.getTestSongWithIdOne());
        Song song = target("/songs/1").request(MediaType.APPLICATION_XML).get(Song.class);
        Assert.assertEquals(1, song.getId().intValue());
        verify(songPersistence, times(1)).getById(1);
    }

    @Test
    public void getSongWithStringIdShouldReturn404() {
        Response response = target("/songs/stringAndNoId").request().get();
        Assert.assertEquals(404, response.getStatus());
        verify(songPersistence, times(0)).getById(anyInt());
    }

    @Test
    public void getSongWithNonExistingIdShouldReturn404() {
        when(songPersistence.getById(999)).thenThrow(new NotFoundException());
        Response response = target("/songs/999").request().get();
        Assert.assertEquals(404, response.getStatus());
        verify(songPersistence, times(1)).getById(999);
    }

    // -------------
    // put Tests
    // -------------

    // JSON
    @Test
    public void updateSongJsonWithNonExistingIdShouldReturn404() {
        Song song = getTestSongWithoutId();

        doThrow(new NoSuchElementException()).when(songPersistence).update(any(Song.class));
        Response response = target("/songs/999").request().put(Entity.json(song));
        Assert.assertEquals(404, response.getStatus());
        verify(songPersistence, times(1)).update(any(Song.class));
    }

    // XML
    @Test
    public void updateSongXmlWithNonExistingIdShouldReturn404() {
        Song song = getTestSongWithoutId();

        doThrow(new NoSuchElementException()).when(songPersistence).update(any(Song.class));
        Response response = target("/songs/999").request().put(Entity.xml(song));
        Assert.assertEquals(404, response.getStatus());
        verify(songPersistence, times(1)).update(any(Song.class));
    }

    // JSON
    @Test
    public void updateSongJsonShouldReturn204AndUpdatedSong() {
        Song songPutTest = getTestSongWithIdOne();

        Song before = new Song.Builder("Can't Stop the Feeling").id(1).artist("Justin Timberlake").album("Trolls").released(2016).build();
        // the original song
        when(songPersistence.getById(1)).thenReturn(before);
        Song songGetTest = target("/songs/1").request(MediaType.APPLICATION_JSON).get(Song.class);
        assertSong(songGetTest, before);

        Response response = target("/songs/1").request().put(Entity.json(songPutTest));
        Assert.assertEquals(204, response.getStatus());


        // the new overwritten song
        when(songPersistence.getById(1)).thenReturn(songPutTest);
        songGetTest = target("/songs/1").request(MediaType.APPLICATION_JSON).get(Song.class);
        assertSong(songGetTest, songPutTest);

        verify(songPersistence, times(2)).getById(1);
        verify(songPersistence, times(1)).update(any(Song.class));
    }

    // XML
    @Test
    public void updateSongXmlShouldReturn204AndUpdatedSong() {
        Song songPutTest = getTestSongWithIdOne();

        Song before = new Song.Builder("Can't Stop the Feeling").id(1).artist("Justin Timberlake").album("Trolls").released(2016).build();
        // the original song
        when(songPersistence.getById(1)).thenReturn(before);
        Song songGetTest = target("/songs/1").request(MediaType.APPLICATION_XML).get(Song.class);
        assertSong(songGetTest, before);

        Response response = target("/songs/1").request().put(Entity.xml(songPutTest));
        Assert.assertEquals(204, response.getStatus());

        // the new overwritten song
        when(songPersistence.getById(1)).thenReturn(songPutTest);
        songGetTest = target("/songs/1").request(MediaType.APPLICATION_XML).get(Song.class);
        assertSong(songGetTest, songPutTest);

        verify(songPersistence, times(2)).getById(1);
        verify(songPersistence, times(1)).update(any(Song.class));
    }

    // JSON
    @Test
    public void updateSongJsonWithDifferentIdInBodyShouldReturn400() {
        Song song = getTestSongWithIdOne();

        Response response = target("/songs/2").request().put(Entity.json(song));
        Assert.assertEquals(400, response.getStatus());
    }

    // XML
    @Test
    public void updateSongXmlWithDifferentIdInBodyShouldReturn400() {
        Song song = getTestSongWithIdOne();

        Response response = target("/songs/2").request().put(Entity.xml(song));
        Assert.assertEquals(400, response.getStatus());
    }

    // JSON
    @Test
    public void updateSongJsonWithNoIdWithinPayloadShouldReturn204AndUpdatedSong() {
        Song songPutTest = getTestSongWithoutId();

        // the original song
        Song before = new Song.Builder("Can't Stop the Feeling").id(1).artist("Justin Timberlake").album("Trolls").released(2016).build();
        when(songPersistence.getById(1)).thenReturn(before);

        Song songGetTest = target("/songs/1").request(MediaType.APPLICATION_JSON).get(Song.class);
        assertSong(songGetTest, before);

        Response response = target("/songs/1").request().put(Entity.json(songPutTest));
        Assert.assertEquals(204, response.getStatus());

        // the new overwritten song
        songPutTest.setId(1);
        when(songPersistence.getById(1)).thenReturn(songPutTest);
        songGetTest = target("/songs/1").request(MediaType.APPLICATION_JSON).get(Song.class);
        assertSong(songGetTest, songPutTest);

        verify(songPersistence, times(2)).getById(1);
        verify(songPersistence, times(1)).update(any(Song.class));
    }

    // XML
    @Test
    public void updateSongXmlWithNoIdWithinPayloadShouldReturn204AndUpdatedSong() {
        Song songPutTest = getTestSongWithoutId();

        // the original song
        Song before = new Song.Builder("Can't Stop the Feeling").id(1).artist("Justin Timberlake").album("Trolls").released(2016).build();
        when(songPersistence.getById(1)).thenReturn(before);

        Song songGetTest = target("/songs/1").request(MediaType.APPLICATION_JSON).get(Song.class);
        assertSong(songGetTest, before);

        Response response = target("/songs/1").request().put(Entity.json(songPutTest));
        Assert.assertEquals(204, response.getStatus());

        // the new overwritten song
        songPutTest.setId(1);
        when(songPersistence.getById(1)).thenReturn(songPutTest);
        songGetTest = target("/songs/1").request(MediaType.APPLICATION_JSON).get(Song.class);
        assertSong(songGetTest, songPutTest);

        verify(songPersistence, times(2)).getById(1);
        verify(songPersistence, times(1)).update(any(Song.class));
    }

    @Test
    public void updateSongWithWrongParamaterShouldReturn404() {
        Song songPutTest = getTestSongWithoutId();

        Response response = target("/songs/garbage").request().put(Entity.xml(songPutTest));
        Assert.assertEquals(404, response.getStatus());
    }

    @Test
    public void updateSongWithWrongPayloadShouldReturn400() {
        Song songPutTest = getTestSongWithWrongPayload();

        Response response = target("/songs/1").request().put(Entity.xml(songPutTest));
        Assert.assertEquals(400, response.getStatus());
    }

    @Test
    public void updateSongJsonJustWithTitleShouldReturn204AndUpdatedSongs() {
        Song songPutTest = getTestSongWithJustATitle();

        // the original song
        Song before = new Song.Builder("Can't Stop the Feeling").id(1).artist("Justin Timberlake").album("Trolls").released(2016).build();
        when(songPersistence.getById(1)).thenReturn(before);

        Song songGetTest = target("/songs/1").request(MediaType.APPLICATION_JSON).get(Song.class);
        assertSong(songGetTest, before);

        Response response = target("/songs/1").request().put(Entity.xml(songPutTest));
        Assert.assertEquals(204, response.getStatus());

        // the new overwritten song
        songPutTest.setId(1);
        when(songPersistence.getById(1)).thenReturn(songPutTest);
        songGetTest = target("/songs/1").request(MediaType.APPLICATION_JSON).get(Song.class);
        assertSong(songGetTest, 1, null, null, "Supermix 2000", null);

        verify(songPersistence, times(2)).getById(1);
        verify(songPersistence, times(1)).update(any(Song.class));
    }
}