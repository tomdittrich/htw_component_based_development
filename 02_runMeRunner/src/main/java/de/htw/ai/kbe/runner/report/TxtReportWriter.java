package de.htw.ai.kbe.runner.report;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Writes an {@link RunMeReport} to a text file
 */
public class TxtReportWriter implements IReportWriter {

    private PrintWriter writer;

    /**
     * Constructor for initializing
     *
     * @param file path
     * @throws ReportWriterException
     */
    public TxtReportWriter(String file) throws ReportWriterException {
        try {
            this.writer = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            throw new ReportWriterException("File " + file + " can not be created.");
        }
    }

    /**
     * Writes the given report into the file
     *
     * @param report to write
     * @throws ReportWriterException
     */
    @Override
    public void writeReport(RunMeReport report) throws ReportWriterException {
        if (report == null) {
            throw new ReportWriterException("Report must not be null");
        }
        writer.write(report.toString());
        writer.close();
    }

}