package de.htw.ai.kbe.runner;

import de.htw.ai.kbe.runner.annotations.RunMe;
import de.htw.ai.kbe.runner.report.RunMeReport;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunMeRunner {

    /**
     * Analyzes a specific class and try to run all methods with the {@link RunMe} annotation.<br>
     * Counts the total number of methods, methods with annotation, methods with annotation but not invokable.<br>
     * The result is a {@link RunMeReport}.
     *
     * @param className the path to the class you want to analyze
     * @return an {@link RunMeReport}
     * @throws RunMeRunnerException
     */
    public static RunMeReport execute(String className) throws RunMeRunnerException {

        if (className == null || className.isEmpty()) {
            throw new RunMeRunnerException("Class name is empty or null.");
        }

        Object instance = null;
        Class<?> c = null;

        try {
            c = Class.forName(className);
            instance = c.newInstance();

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new RunMeRunnerException("Can not instantiate " + className + ": " + e.getMessage());
        }

        List<String> methodNamesWithRunMe = new ArrayList<>();
        List<String> methodNamesWithNotInvokable = new ArrayList<>();
        Method[] methods = c.getDeclaredMethods();

        System.out.println("Total methods: " + methods.length);

        for (Method method : methods) {

            String methodName = method.getName();

            System.out.println("Current method: " + methodName);

            if (method.isAnnotationPresent(RunMe.class)) {
                try {
                    methodNamesWithRunMe.add(methodName);
                    System.out.println("  Try to invoke");
                    method.invoke(instance);
                    System.out.println("  Invocation successful");

                } catch (Exception e) {
                    methodNamesWithNotInvokable.add(methodName);
                    System.out.println("  Not invokable: " + e.getMessage());
                }
            }
        }

        return new RunMeReport(methodNamesWithRunMe, methodNamesWithNotInvokable, methods.length, new Date());
    }
}
