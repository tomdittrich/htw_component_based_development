package de.htw.ai.kbe.runner.annotations;

import java.lang.annotation.*;

/**
 * Annotation for classes to be executed
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface RunMe {

}