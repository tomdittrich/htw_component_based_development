package de.htw.ai.kbe.runner;

import de.htw.ai.kbe.properties.PropsFileReadException;
import de.htw.ai.kbe.properties.PropsFileUtil;
import de.htw.ai.kbe.runner.config.CLIHandler;
import de.htw.ai.kbe.runner.config.ConfigException;
import de.htw.ai.kbe.runner.config.IConfig;
import de.htw.ai.kbe.runner.config.IConfigProvider;
import de.htw.ai.kbe.runner.report.IReportWriter;
import de.htw.ai.kbe.runner.report.ReportWriterException;
import de.htw.ai.kbe.runner.report.RunMeReport;
import de.htw.ai.kbe.runner.report.TxtReportWriter;

import java.util.Properties;

/**
 * Main App to read-in a properties file, reflect a class and write an report
 */
public class Main {

    public static void main(String[] args) {
        IConfigProvider configProvider;
        IConfig config = null;
        try {
            configProvider = CLIHandler.init(args);
            config = configProvider.getConfig();
        } catch (ConfigException e) {
            System.err.println("Configuration invalid.");
            System.exit(-1);
        }

        Properties props = null;
        try {
            props = PropsFileUtil.readPropsFile(config.getPropertiesFile());
        } catch (PropsFileReadException e) {
            System.err.println("Failed to read properties file. " + e.getMessage());
            System.exit(-3);
        }

        RunMeReport report = null;
        try {
            report = RunMeRunner.execute(props.getProperty("classToRun"));

        } catch (RunMeRunnerException e) {
            System.err.println("Execution failed! " + e.getMessage());
            System.exit(-2);
        }

        try {
            IReportWriter writer = new TxtReportWriter(config.getOutputFile());
            writer.writeReport(report);
        } catch (ReportWriterException e) {
            System.err.println("Failed to writer report: " + e.getMessage());
            System.out.println("Report: " + report);
        }
    }
}