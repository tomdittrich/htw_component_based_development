package de.htw.ai.kbe.runner.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * An report about the method reflection of a class
 */
public class RunMeReport {

    private List<String> methodNamesWithRunMe = new ArrayList<>();
    private List<String> methodNamesWithNotInvokable = new ArrayList<>();
    private int methodCount = 0;
    private Date date;

    /**
     * Constructor to initialize the report
     *
     * @param methodNamesWithRunMe
     * @param methodNamesWithNotInvokable
     * @param methodCount
     * @param date
     */
    public RunMeReport(List<String> methodNamesWithRunMe, List<String> methodNamesWithNotInvokable, int methodCount, Date date) {
        this.methodNamesWithRunMe = methodNamesWithRunMe;
        this.methodNamesWithNotInvokable = methodNamesWithNotInvokable;
        this.methodCount = methodCount;
        this.date = date;
    }

    public List<String> getMethodNamesWithRunMe() {
        return methodNamesWithRunMe;
    }

    public List<String> getMethodNamesWithNotInvokable() {
        return methodNamesWithNotInvokable;
    }

    public int getMethodCount() {
        return methodCount;
    }

    public Date getDate() {
        return date;
    }

    /**
     * @return report as String to print
     */
    @Override
    public String toString() {
        return String.format("RunMeReport %s%nTotal methods: %d%n@RunMe methods: %d%nFailed invocations for: %s",
                getDate(),
                getMethodCount(),
                getMethodNamesWithRunMe().size(),
                String.join(", ", getMethodNamesWithNotInvokable())
        );
    }
}