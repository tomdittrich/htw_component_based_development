package de.htw.ai.kbe.servlet.utils;

import java.util.List;

/**
 * Iterates a list of entries and returns the toString-method
 * for every single element within the list
 *
 * @version 0.5
 */
public class ListEntriesToString {

    /**
     * Calling every toString() for every element and appending it
     * to a whole one string. You can separate the entries with a
     * String of your choice.
     *
     * @param list             list to iterate
     * @param separationString string(s) to separate the entries
     * @return the combined string
     */
    public static <T> String toString(List<T> list, String separationString) {
        StringBuilder resultString = new StringBuilder();
        for (T entryInList : list) {
            resultString.append(entryInList).append(separationString);
        }
        return resultString.toString();
    }
}
