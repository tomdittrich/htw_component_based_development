package de.htw.ai.kbe.servlet.utils;

public abstract class Constants {
    public final static String INIT_PARAM_DATASOURCE = "datasource";
    public final static String QUERY_PARAM_ALL = "all";
    public final static String QUERY_PARAM_SONGID = "songId";
    
    public final static String HEADER_ACCEPT = "Accept";
    public final static String HEADER_CONTENT_TYPE = "Content-Type";
    
    public final static String CONTENTTYPE_JSON = "application/json";
    public final static String CONTENTTYPE_XML = "application/xml";
    public final static String CONTENTTYPE_TEXT = "text/plain";
}
