package de.htw.ai.kbe.servlet.marshalling;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import de.htw.ai.kbe.servlet.pojo.Song;

public interface IMarshaller {

    /**
     * Reads a list of songs from into List<Song>
     *
     * @param InputStream to read from
     * @return a list of songs
     * @throws MarshallingException
     */
    List<Song> readSongsFromStream(InputStream is) throws MarshallingException;
    
    /**
     * Read a single song
     *
     * @param InputStream to read from
     * @return a list of songs
     * @throws MarshallingException
     */
    Song readSongFromStream(InputStream is) throws MarshallingException;
    
    /**
     * Write a List<Song> to an outputstream
     *
     * @param songs list with songs to store
     * @param os OutputStream
     * @throws MarshallingException
     */
    void writeSongsToStream(List<Song> songs, OutputStream os) throws MarshallingException;
}
