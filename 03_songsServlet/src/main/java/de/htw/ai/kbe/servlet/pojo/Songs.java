package de.htw.ai.kbe.servlet.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.htw.ai.kbe.servlet.utils.ListEntriesToString;

/**
 * Songs class for XML songs element (root element)
 *
 * @version 0.5
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Songs {

    @XmlElement(name = "song")
    private List<Song> songList;

    public List<Song> getSongList() {
        return this.songList;
    }

    public void setSongList(List<Song> song) {
        this.songList = song;
    }

    /**
     * Outputs the whole songs with details
     *
     * @return the whole songs-string
     */
    @Override
    public String toString() {
        return ListEntriesToString.toString(this.songList,"\n");
    }
}