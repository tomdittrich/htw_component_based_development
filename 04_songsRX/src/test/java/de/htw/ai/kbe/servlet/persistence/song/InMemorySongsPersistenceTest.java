package de.htw.ai.kbe.servlet.persistence.song;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static de.htw.ai.kbe.servlet.utils.TestUtils.assertSong;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.NotFoundException;

import org.junit.Before;
import org.junit.Test;

import de.htw.ai.kbe.servlet.persistence.song.InMemorySongsPersistence;
import de.htw.ai.kbe.servlet.pojo.Song;

public class InMemorySongsPersistenceTest {
    
    private static Song[] testSongs = {
        new Song.Builder("Baker Street")
        .id(0)
        .artist("Gerry Rafferty")
        .album("City To City")
        .released(1978)
        .build(),
        new Song.Builder("Down To The River")
        .id(1)
        .artist("Welshly Arms")
        .album("No Place Is Home")
        .released(2018)
        .build(),
        
    };

    InMemorySongsPersistence persistance;

    @Before
    public void setUp() throws Exception {
        persistance = new InMemorySongsPersistence(Arrays.asList(testSongs));
    }

    @Test
    public void testGetAll() {
        List<Song> actual = persistance.getAll();
        assertEquals(testSongs.length, actual.size());
        assertThat(actual, is(Arrays.asList(testSongs)));
    }

    @Test
    public void testGetByIdShouldReturnSong() {
        Song actual = persistance.getById(0);
        Song expected = testSongs[0];
        assertNotNull(actual);
        assertSong(expected, actual);
    }
    
    @Test(expected=NotFoundException.class)
    public void testGetByIdWithWrongIdShouldThrowException() {
        persistance.getById(testSongs.length + 1);
    }

    @Test
    public void testDelete() {
        assertEquals(testSongs.length, persistance.getAll().size());
        persistance.delete(0);
        assertEquals(testSongs.length - 1, persistance.getAll().size());
    }
    
    @Test(expected=NotFoundException.class)
    public void testDeleteShouldThrowException() {
        assertEquals(testSongs.length, persistance.getAll().size());
        persistance.delete(-1);
    }

    @Test
    public void testAdd() {
        assertEquals(testSongs.length, persistance.getAll().size());
        
        Song newSong = new Song.Builder("Ketamine")
                .album("Nocturnal - Bonus Tracks")
                .artist("Razz")
                .released(2018).build();
        
        Integer newId = persistance.add(newSong);
        
        assertEquals(testSongs.length + 1, persistance.getAll().size());
        assertEquals(testSongs.length, newId.intValue());
        assertEquals(newId, newSong.getId());
    }

    @Test
    public void testUpdate() {
        Song toUpdate = copySong(testSongs[1]);
        toUpdate.setArtist("Tester");
        persistance.update(toUpdate);
        Song updated = persistance.getById(1);
        assertSong(toUpdate, updated);
    }

    @Test(expected=NotFoundException.class)
    public void testUpdateShouldThrowException() {
        Song toUpdate = copySong(testSongs[1]);
        toUpdate.setId(testSongs.length + 200);
        toUpdate.setArtist("Tester");
        persistance.update(toUpdate);
    }
    
    private Song copySong(Song song) {
        return new Song.Builder(song.getTitle())
            .id(song.getId())
            .artist(song.getArtist())
            .album(song.getAlbum())
            .released(song.getReleased())
            .build();
    }
}
