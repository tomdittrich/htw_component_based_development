package de.htw.ai.kbe.servlet.resources;

import static org.junit.Assert.*;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import de.htw.ai.kbe.servlet.business.AuthenticationServiceImpl;
import de.htw.ai.kbe.servlet.business.IAuthenticationService;
import de.htw.ai.kbe.servlet.persistence.InMemoryUserPersistence;
import de.htw.ai.kbe.servlet.persistence.user.IUserPersistence;
import de.htw.ai.kbe.servlet.utils.Constants;

public class AuthResourceTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(AuthResource.class).register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(AuthenticationServiceImpl.class).to(IAuthenticationService.class);
                bind(new InMemoryUserPersistence()).to(IUserPersistence.class);
            }
        });
    }
    
    @Test
    public void testGetWithExistingUserShouldReturnToken() {
        Response response = target(Constants.AUTH_RESOURCE_PATH).queryParam("userId", "ctester").request().get();
        
        assertEquals(200, response.getStatus());
        String token = response.readEntity(String.class);
        assertNotNull(token);
        System.out.println(token);        
    }

    @Test
    public void testGetWithNonExistingUserShouldReturnUnauthorized() {
        Response response = target(Constants.AUTH_RESOURCE_PATH).queryParam("userId", "hackerman").request().get();
        
        assertEquals(401, response.getStatus());
        String token = response.readEntity(String.class);
        assertTrue(token == null || token.isEmpty());
    }

    @Test
    public void testGetWithoutUserIdShouldReturnBadRequest() {
        Response response = target(Constants.AUTH_RESOURCE_PATH).request().get();
        
        assertEquals(400, response.getStatus());
        String token = response.readEntity(String.class);
        assertTrue(token == null || token.isEmpty());
    }
    
    @Test
    public void testGetWithEmptyUserIdShouldReturnBadRequest() {
        Response response = target(Constants.AUTH_RESOURCE_PATH).queryParam("userId", "").request().get();
        
        assertEquals(400, response.getStatus());
        String token = response.readEntity(String.class);
        assertTrue(token == null || token.isEmpty());
    }
}
