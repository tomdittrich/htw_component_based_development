package de.htw.ai.kbe.servlet.persistence.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import de.htw.ai.kbe.servlet.pojo.User;
import javassist.NotFoundException;

public class JSONUserPersistenceTest {
    
    private static JSONUserPersistence persistence;
    
    private static final User TEST_USER = new User();
    static {
        TEST_USER.setId(1);
        TEST_USER.setUserId("mmuster");
        TEST_USER.setLastName("Muster");
        TEST_USER.setFirstName("Maxime");
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        persistence = new JSONUserPersistence(new File(JSONUserPersistenceTest.class.getResource("users.json").getFile()));
    }

    @Test
    public void testGetAllUsersShouldParseJSON() {
        List<User> users = persistence.getAllUsers();
        
        
        assertEquals(1,  users.size());
        
        User user = users.get(0);
        assertUser(TEST_USER, user);
    }

    @Test
    public void testGetUserByIdShouldReturnUser() throws NotFoundException {
        User user = persistence.getByUserId("mmuster");
        assertNotNull(user);
        assertUser(TEST_USER, user);
    }
    
    @Test(expected=NotFoundException.class)
    public void testGetUserByIdShouldThrowNotFound() throws NotFoundException {
        User user = persistence.getByUserId("notfound");
        assertNull(user);
    }
    
    private void assertUser(User expected, User actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getUserId(), actual.getUserId());

    }

}
