package de.htw.ai.kbe.servlet.business;

import javax.ws.rs.NotAuthorizedException;

/**
 * Interface for user authentication
 * @author marcel
 *
 */
public interface IAuthenticationService {

    /**
     * Authenticate a user. On successful authentication a token will be generated
     * @param userId userId to authenticate
     * @return token
     * @throws NotAuthorizedException
     */
    public String authenticate(String userId) throws NotAuthorizedException;
    
    /**
     * Check whether the token is valid
     * @param token to check
     * @return boolean for validity
     */
    public boolean isValid(String token);
}
