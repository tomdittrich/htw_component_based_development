package de.htw.ai.kbe.servlet.resources;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import de.htw.ai.kbe.servlet.persistence.song.ISongPersistence;
import de.htw.ai.kbe.servlet.pojo.Song;
import de.htw.ai.kbe.servlet.utils.Constants;

/**
 * Servlet to read and write Songs into a simple kind of database
 *
 * @version 0.1
 */
@Path(Constants.SONGS_RESOURCE_PATH)
public class SongsResource {

    private static final Logger LOG = Logger.getLogger(SongsResource.class);

    @Context
    UriInfo uriInfo;
    
    @Inject
    private ISongPersistence songsPersistence;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Song> getAllSongs() {
        LOG.info("Get all songs");
        return songsPersistence.getAll();
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getSong(@PathParam("id") Integer id) {
        LOG.info("Get song by id (" + id + ")");

        Song song = songsPersistence.getById(id);
        return Response.ok(song).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces(MediaType.TEXT_PLAIN)
    public Response createSong(@Valid Song song) {
        LOG.info("Creating new song");
        Integer newId = songsPersistence.add(song);
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(newId.toString());
        return Response.created(uriBuilder.build()).entity("Song added (new id: " + newId + ")").build();
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}")
    public Response updateSong(@PathParam("id") Integer id, @Valid Song song) {
        LOG.info("Updating song (id " + id + ")");

        if(song.getId() != null && id != song.getId()){
            throw new BadRequestException("Id missmatch between argument and payload");
        }

        song.setId(id);
        songsPersistence.update(song);
        return Response.status(Status.NO_CONTENT).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Integer id) {
        LOG.info("Deleting song (id " + id + ")");

        songsPersistence.delete(id);
        return Response.status(Status.NO_CONTENT).build();
    }
}
