package de.htw.ai.kbe.servlet.resources.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import de.htw.ai.kbe.servlet.business.IAuthenticationService;
import de.htw.ai.kbe.servlet.utils.Constants;

@Provider
public class TokenFilter implements ContainerRequestFilter {

    @Inject
    private IAuthenticationService authService;
    
    @Override
    public void filter(ContainerRequestContext req) throws IOException {
        String path = req.getUriInfo().getPath();
        if(path.contains(Constants.AUTH_RESOURCE_PATH.substring(1))) {
            return;
        }
        
        String token = req.getHeaderString(Constants.AUTH_HEADER);
        
        boolean valid = authService.isValid(token);
        
        if(!valid) {
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
    }

}
