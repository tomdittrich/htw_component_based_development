package de.htw.ai.kbe.servlet.di;

import javax.inject.Singleton;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

import de.htw.ai.kbe.servlet.business.AuthenticationServiceImpl;
import de.htw.ai.kbe.servlet.business.IAuthenticationService;
import de.htw.ai.kbe.servlet.persistence.song.ISongPersistence;
import de.htw.ai.kbe.servlet.persistence.song.InMemorySongsPersistence;
import de.htw.ai.kbe.servlet.persistence.user.IUserPersistence;
import de.htw.ai.kbe.servlet.persistence.user.JSONUserPersistence;

public class DependencyBinder extends AbstractBinder {

    @Override
    protected void configure() {
        bind(InMemorySongsPersistence.class).to(ISongPersistence.class).in(Singleton.class);
        bind(JSONUserPersistence.getInstance()).to(IUserPersistence.class);
        bind(AuthenticationServiceImpl.class).to(IAuthenticationService.class).in(Singleton.class);
    }

}
